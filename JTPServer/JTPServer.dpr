program JTPServer;
{$APPTYPE GUI}

{$R *.dres}

uses
  Vcl.Forms,
  Web.WebReq,
  IdHTTPWebBrokerBridge,
  fJTPServerMain in 'fJTPServerMain.pas' {JTPServerMainForm},
  mScene in 'mScene.pas' {VideoSettingsFuncs: TDataModule},
  mWeb in 'mWeb.pas' {WebFuncs: TWebModule},
  uJSONUtils in 'uJSONUtils.pas',
  frTrunkSettings in 'frTrunkSettings.pas' {TrunkSettingsFrame: TFrame},
  frCaptureSettings in 'frCaptureSettings.pas' {CaptureSettingsFrame: TFrame},
  uVideoSettings in 'uVideoSettings.pas',
  uJTPUtils in 'uJTPUtils.pas',
  fVideoOutForm in 'fVideoOutForm.pas' {VideoOutForm1};

{$R *.res}

begin
  if WebRequestHandler <> nil then
    WebRequestHandler.WebModuleClass := WebModuleClass;
  Application.Initialize;
  Application.CreateForm(TJTPServerMainForm, JTPServerMainForm);
  Application.Run;
end.

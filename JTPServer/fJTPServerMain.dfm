object JTPServerMainForm: TJTPServerMainForm
  Left = 271
  Top = 114
  BorderStyle = bsDialog
  Caption = 'JTP Studio Server'
  ClientHeight = 367
  ClientWidth = 612
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object StartStopBtn: TButton
    Left = 440
    Top = 324
    Width = 121
    Height = 25
    TabOrder = 0
    OnClick = StartStopBtnClick
  end
  object OutGroup: TGroupBox
    Left = 8
    Top = 8
    Width = 387
    Height = 345
    Caption = '  '#1042#1080#1076#1077#1086#1074#1099#1074#1086#1076'  '
    TabOrder = 1
    inline TSF1: TTrunkSettingsFrame
      Left = 14
      Top = 16
      Width = 176
      Height = 148
      TabOrder = 0
      ExplicitLeft = 14
      ExplicitTop = 16
    end
    inline TSF2: TTrunkSettingsFrame
      Left = 196
      Top = 16
      Width = 176
      Height = 148
      TabOrder = 1
      ExplicitLeft = 196
      ExplicitTop = 16
    end
    inline TSF3: TTrunkSettingsFrame
      Left = 14
      Top = 160
      Width = 176
      Height = 148
      TabOrder = 2
      ExplicitLeft = 14
      ExplicitTop = 160
    end
    inline TSF4: TTrunkSettingsFrame
      Left = 196
      Top = 160
      Width = 176
      Height = 148
      TabOrder = 3
      ExplicitLeft = 196
      ExplicitTop = 160
    end
    object OutSimBox: TCheckBox
      Left = 22
      Top = 309
      Width = 338
      Height = 17
      Caption = #1057#1080#1084#1091#1083#1103#1090#1086#1088' '#1074#1099#1074#1086#1076#1072
      TabOrder = 4
      OnClick = OutSimBoxClick
    end
  end
  object GroupBox1: TGroupBox
    Left = 400
    Top = 8
    Width = 201
    Height = 193
    Caption = '  '#1042#1080#1076#1077#1086#1079#1072#1093#1074#1072#1090'  '
    Enabled = False
    TabOrder = 2
    inline CaptureSettingsFrame1: TCaptureSettingsFrame
      Left = 16
      Top = 16
      Width = 176
      Height = 148
      TabOrder = 0
      ExplicitLeft = 16
      ExplicitTop = 16
    end
  end
  object ServerGroup: TGroupBox
    Left = 400
    Top = 208
    Width = 201
    Height = 105
    Caption = '  '#1057#1077#1088#1074#1077#1088'  '
    TabOrder = 3
    object Label1: TLabel
      Left = 20
      Top = 31
      Width = 69
      Height = 13
      Caption = #1055#1086#1088#1090' '#1089#1077#1088#1074#1077#1088#1072
    end
    object EditPort: TEdit
      Left = 104
      Top = 28
      Width = 73
      Height = 21
      TabOrder = 0
      Text = '55555'
    end
    object ButtonOpenBrowser: TButton
      Left = 40
      Top = 64
      Width = 121
      Height = 25
      Caption = #1054#1090#1082#1088#1099#1090#1100' '#1073#1088#1072#1091#1079#1077#1088
      TabOrder = 1
      OnClick = ButtonOpenBrowserClick
    end
  end
end

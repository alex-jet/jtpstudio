unit fJTPServerMain;
{$i ..\JTPStudio\JTPProjects.inc}

interface

uses
  Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.AppEvnts, Vcl.StdCtrls, IdHTTPWebBrokerBridge, Web.HTTPApp,
  frTrunkSettings, frCaptureSettings, uVideoSettings,
  TypesJTP, JTPStudio, Decklink, Vcl.ExtCtrls, Vcl.WinXCtrls;

resourcestring
  rsStart = '���������';
  rsStop = '����������';

type
  TJTPServerMainForm = class(TForm)
    StartStopBtn: TButton;
    OutGroup: TGroupBox;
    TSF1: TTrunkSettingsFrame;
    TSF2: TTrunkSettingsFrame;
    TSF3: TTrunkSettingsFrame;
    TSF4: TTrunkSettingsFrame;
    OutSimBox: TCheckBox;
    GroupBox1: TGroupBox;
    CaptureSettingsFrame1: TCaptureSettingsFrame;
    ServerGroup: TGroupBox;
    Label1: TLabel;
    EditPort: TEdit;
    ButtonOpenBrowser: TButton;
    procedure FormCreate(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure StartStopBtnClick(Sender: TObject);
    procedure ButtonOpenBrowserClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OutSimBoxClick(Sender: TObject);
  private
    FServer: TIdHTTPWebBrokerBridge;
    FVSObj: TVideoSettingsObj;
    FTrunkFrames: array[1..4] of TTrunkSettingsFrame;
    procedure AdjustUI;
    procedure StartServer;
    procedure StopServer;
  public
  end;

var
  JTPServerMainForm: TJTPServerMainForm;

implementation

{$R *.dfm}

uses
  WinApi.Windows, Winapi.ShellApi, Datasnap.DSSession;

procedure TJTPServerMainForm.AdjustUI;
begin
  if FServer.Active
    then StartStopBtn.Caption := rsStop
    else StartStopBtn.Caption := rsStart;
end;

procedure TJTPServerMainForm.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
begin
  {ButtonStart.Enabled := not FServer.Active;
  ButtonStop.Enabled := FServer.Active;
  EditPort.Enabled := not FServer.Active;}
end;

procedure TJTPServerMainForm.ButtonOpenBrowserClick(Sender: TObject);
var
  LURL: string;
begin
  //StartServer;
  LURL := Format('http://localhost:%s', [EditPort.Text]);
  ShellExecute(0,
        nil,
        PChar(LURL), nil, nil, SW_SHOWNOACTIVATE);
end;

procedure TJTPServerMainForm.StartStopBtnClick(Sender: TObject);
begin
  if FServer.Active
    then StopServer
    else StartServer;
end;

procedure TerminateThreads;
begin
  if TDSSessionManager.Instance <> nil then
    TDSSessionManager.Instance.TerminateAllSessions;
end;

procedure TJTPServerMainForm.FormCreate(Sender: TObject);
var
  i: integer;
  //DevL: TStringList;
  ModesL: TStringList;
begin
  StartStopBtn.Caption := rsStart;

  FTrunkFrames[1] := TSF1;
  FTrunkFrames[2] := TSF2;
  FTrunkFrames[3] := TSF3;
  FTrunkFrames[4] := TSF4;

  FVSObj := TVideoSettingsObj.Create;

  //DevL := TStringList.Create;
  ModesL := TStringList.Create;
  try
    //FVSObj.GetVideoDevicesList(SIMULATOR_VIDEO_DEVICE, DevL);
    FVSObj.GetVideoModesList(ModesL);
    for i:=1 to 4 do
      with FTrunkFrames[i] do
        begin
          TrunkIndex := i;
          TrunkActivateBox.Checked := (i < 3);
          //DevicesList.Items.Assign(DevL);
          //DevicesList.ItemIndex := 0;
          ModesList.Items.Assign(ModesL);
          ModesList.ItemIndex := 0;
        end;
    OutSimBox.Checked := True;
  finally
    ModesL.Free;
    //DevL.Free;
  end;

  FServer := TIdHTTPWebBrokerBridge.Create(Self);
end;

procedure TJTPServerMainForm.FormDestroy(Sender: TObject);
begin
  StopServer;
  FVSObj.Free;
end;

procedure TJTPServerMainForm.OutSimBoxClick(Sender: TObject);
var
  i: integer;
  DevType: dword;
  DevL: TStringList;
begin
  DevL := TStringList.Create;
  try
    if OutSimBox.Checked
      then DevType := SIMULATOR_VIDEO_DEVICE
      else DevType := DECKLINK_VIDEO_DEVICE;
    FVSObj.GetVideoDevicesList(DevType, DevL);
    for i:=1 to 4 do
      with FTrunkFrames[i] do
        begin
          DevicesList.Items.Assign(DevL);
          DevicesList.ItemIndex := 0;
        end;
  finally
    DevL.Free;
  end;
end;

procedure TJTPServerMainForm.StartServer;
var
  VSRec: TVideoSettingsRec;
  i: integer;
begin
  VSRec.OutSim := OutSimBox.Checked;
  for i:=1 to 4 do
    begin
      VSRec.TrunkSets[i].Active := FTrunkFrames[i].TrunkActivateBox.Checked;
      VSRec.TrunkSets[i].DeviceIdx := FTrunkFrames[i].DevicesList.ItemIndex;
      VSRec.TrunkSets[i].ModeIdx := FTrunkFrames[i].ModesList.ItemIndex;
      VSRec.TrunkSets[i].KeyPass := FTrunkFrames[i].PassBox.Checked;
    end;
  FVSObj.InitVideo(VSRec);

  if not FServer.Active then
    begin
      FServer.Bindings.Clear;
      FServer.DefaultPort := StrToInt(EditPort.Text);
      FServer.KeepAlive := true;
      FServer.AutoStartSession := true;
      FServer.Active := True;
    end;

  AdjustUI;
end;

procedure TJTPServerMainForm.StopServer;
begin
  FVSObj.StopEngine;

  if FServer.Active then
    begin
      TerminateThreads;
      FServer.Active := False;
      FServer.Bindings.Clear;
    end;

  AdjustUI;
end;

end.

unit fVideoOutForm;
{$i ..\JTPStudio\JTPProjects.inc}

interface

uses
  Winapi.Windows, Winapi.Messages,
  System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.ExtCtrls;

type
  TVideoOutForm1 = class(TForm)
    Panel1: TPanel;
    StatusBar1: TStatusBar;
  private
    function GetVideoOutHandle: HWND;
  public
    property VideoOutHandle: HWND read GetVideoOutHandle;
  end;

implementation

{$R *.dfm}

{ TVideoOutForm1 }

function TVideoOutForm1.GetVideoOutHandle: HWND;
begin
  Result := Panel1.Handle;
end;

end.

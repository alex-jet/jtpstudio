object CaptureSettingsFrame: TCaptureSettingsFrame
  Left = 0
  Top = 0
  Width = 176
  Height = 148
  TabOrder = 0
  object CaptureActivateBox: TCheckBox
    Left = 8
    Top = 3
    Width = 156
    Height = 17
    Caption = #1040#1082#1090#1080#1074#1080#1088#1086#1074#1072#1090#1100' '#1079#1072#1093#1074#1072#1090
    TabOrder = 0
  end
  object DevicesList: TComboBox
    Left = 19
    Top = 32
    Width = 145
    Height = 21
    Style = csDropDownList
    TabOrder = 1
  end
  object ModesList: TComboBox
    Left = 19
    Top = 72
    Width = 145
    Height = 21
    Style = csDropDownList
    TabOrder = 2
  end
  object CaptureSimBox: TCheckBox
    Left = 19
    Top = 112
    Width = 145
    Height = 17
    Caption = #1057#1080#1084#1091#1083#1103#1090#1086#1088' '#1079#1072#1093#1074#1072#1090#1072
    TabOrder = 3
  end
end

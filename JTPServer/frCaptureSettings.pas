unit frCaptureSettings;
{$i ..\JTPStudio\JTPProjects.inc}

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TCaptureSettingsFrame = class(TFrame)
    CaptureActivateBox: TCheckBox;
    DevicesList: TComboBox;
    ModesList: TComboBox;
    CaptureSimBox: TCheckBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

end.

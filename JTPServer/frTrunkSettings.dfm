object TrunkSettingsFrame: TTrunkSettingsFrame
  Left = 0
  Top = 0
  Width = 176
  Height = 148
  TabOrder = 0
  object TrunkActivateBox: TCheckBox
    Left = 8
    Top = 3
    Width = 156
    Height = 17
    TabOrder = 0
    OnClick = TrunkActivateBoxClick
  end
  object DevicesList: TComboBox
    Left = 19
    Top = 32
    Width = 145
    Height = 21
    Style = csDropDownList
    TabOrder = 1
    Visible = False
  end
  object ModesList: TComboBox
    Left = 19
    Top = 72
    Width = 145
    Height = 21
    Style = csDropDownList
    TabOrder = 2
    Visible = False
  end
  object PassBox: TCheckBox
    Left = 19
    Top = 112
    Width = 145
    Height = 17
    Caption = #1053#1072' '#1087#1088#1086#1093#1086#1076
    TabOrder = 3
    Visible = False
  end
end

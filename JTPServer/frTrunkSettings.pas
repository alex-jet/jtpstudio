unit frTrunkSettings;
{$i ..\JTPStudio\JTPProjects.inc}

interface

uses
  Winapi.Windows, Winapi.Messages,
  System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

resourcestring
  rsVideoOut = '���������� ';

type
  TTrunkSettingsFrame = class(TFrame)
    TrunkActivateBox: TCheckBox;
    DevicesList: TComboBox;
    ModesList: TComboBox;
    PassBox: TCheckBox;
    procedure TrunkActivateBoxClick(Sender: TObject);
  private
    FTrunkIndex: integer;
    procedure SetTrunkIndex(const Value: integer);
  public
    property TrunkIndex: integer read FTrunkIndex write SetTrunkIndex;
  published
  end;

implementation

{$R *.dfm}

{ TTrunkSettingsFrame }

procedure TTrunkSettingsFrame.SetTrunkIndex(const Value: integer);
begin
  if (Value <= 0) or (Value = FTrunkIndex)
    then Exit;
  TrunkActivateBox.Caption := rsVideoOut + IntToStr(Value);
  FTrunkIndex := Value;
end;

procedure TTrunkSettingsFrame.TrunkActivateBoxClick(Sender: TObject);
begin
  DevicesList.Visible := TrunkActivateBox.Checked;
  ModesList.Visible := TrunkActivateBox.Checked;
  PassBox.Visible := TrunkActivateBox.Checked;
end;

end.

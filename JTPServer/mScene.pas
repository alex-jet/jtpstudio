﻿unit mScene;
{$i ..\JTPStudio\JTPProjects.inc}

interface

uses
  WinAPI.Windows,
  System.SysUtils, System.Classes, System.Json,
  Datasnap.DSServer, Datasnap.DSAuth,
  TypesJTP, JTPStudio, Decklink,
  uJTPUtils, uJSONUtils;

type
{$METHODINFO ON}
  TScene = class(TDataModule)
  private
    FProjPath: array[0..1] of TPath;
    FTextures: array[0..4095] of TPath;
  public
    function LoadProject(const APath: string): TJTPJsonResult;
    function Play(const AName: string; const ATrunk: integer): TJTPJsonResult;
    function Close(const AName: string; const ATrunk: integer): TJTPJsonResult;
  end;
{$METHODINFO OFF}

implementation

{$R *.dfm}

uses
  System.AnsiStrings;

{ TScene }

function TScene.Close(const AName: string; const ATrunk: integer): TJTPJsonResult;
var
  Res: UInt64;
  ErrData: TJtpFuncData;
  ErrStr: string;
  scName: string;
  sceneName: TName;
begin
  if not ((ATrunk > 0) and (ATrunk < 5)) then
    begin
      Res := JTP_INTERNAL_ERROR;
      ErrStr := 'Internal error';
      Result := TJsonUtils.StringToResult(Res, ErrStr, 'CloseScene');
      Exit;
    end;

  scName := AName + '_Trunk_' + IntToStr(ATrunk);
  TJTPUtils.StringToAnsiArray(scName, @sceneName.text[0], 63);

  OpenRecords(0, nil);
  try
  	Res := CloseScene(sceneName.text, FLT_UNDEF, @ErrData);
    ErrStr := TJTPUtils.GetErrorString(Res, ErrData.pErrorsStr);
    Result := TJsonUtils.StringToResult(Res, ErrStr, 'CloseScene');
  finally
    CloseRecords(0, nil);
  end;
end;

function TScene.LoadProject(const APath: string): TJTPJsonResult;
var
  Res: UInt64;
  ErrData: TJtpFuncData;
  ErrStr: string;
begin
  TJTPUtils.StringToAnsiArray(APath, @FProjPath[0].text, 255);
  FProjPath[1].text := '';

  //  Пути к картинкам.
  FTextures[0].Text := 'Resources/Fotos/Foto1.jpg';
  FTextures[1].Text := 'Resources/Fotos/Foto2.jpg';
  FTextures[2].Text := 'Resources/Fotos/Foto3.jpg';
  FTextures[3].Text := '';

  Res := JTPStudio.LoadProject(@FProjPath, @FTextures, 0, @ErrData);
  if Res <> JTP_OK then
    begin
  	  ErrStr := TJTPUtils.GetErrorString(Res, ErrData.pErrorsStr);
      Result := TJsonUtils.StringToResult(Res, ErrStr, 'LoadProject');
      Exit;
    end;

  Res := JTPStudio.StartEngine(1, 0, @ErrData);
  ErrStr := TJTPUtils.GetErrorString(Res, ErrData.pErrorsStr);
  Result := TJsonUtils.StringToResult(Res, ErrStr, 'StartEngine');
end;

function TScene.Play(const AName: string; const ATrunk: integer): TJTPJsonResult;
var
  Res: UInt64;
  ErrStr: string;
  sceneName: TName;
  PlaySceneData: TPlaySceneData;
  CloneObjectData: TCloneObjectData;
  ErrData: TJtpFuncData;
  Delay: single;
  VPos: TVector;
  TableScene1: TName;
  StringObjects : array[0..31] of TName;
  i: integer;
  Text: string;
  TextIter: string;
begin
  TJTPUtils.StringToAnsiArray(AName, @sceneName.text[0], 63);

  PlaySceneData.Create;
  OpenRecords(0, nil);

  try
    if (ATrunk > 0) and (ATrunk < 5) then
      begin
        Res := PlayScene(@sceneName.text[0], 0.0, 0, ATrunk, @PlaySceneData);
        if Res <> JTP_OK then
          begin
            ErrStr := TJTPUtils.GetErrorString(Res, PlaySceneData.pErrorsStr);
            Result := TJsonUtils.StringToResult(Res, ErrStr, 'PlayScene');
            Exit;
          end;

        System.AnsiStrings.StrCopy(TableScene1.text, PlaySceneData.SceneName.text);
        Delay := 0.2;
        VPos.VSet(0.0, -12.0, 0.0);
        StringObjects[0].text := 'String1';

        //  Создадим новые копии строк.
        for i:=1 to 4 do
          begin
            Res := JTPStudio.CloneObject(TableScene1.text, 'String1', @VPos, nil, nil, nil, Delay, JTP_RELATIVE, @CloneObjectData);
            if Res = JTP_OK then StringObjects[i].text := CloneObjectData.ObjectName.text
                            else
              begin
                ErrStr := TJTPUtils.GetErrorString(Res, CloneObjectData.pErrorsStr);
                Result := TJsonUtils.StringToResult(Res, ErrStr, 'CloneObject');
                Exit;
              end;

            VPos.y := VPos.y - 12.0;
            Delay := Delay + 0.2;
          end;

        //  Обновим текст на строках.
        Text := 'Der Bestätigungstext ';

        for i:=0 to 4 do
          begin
            TextIter := Text + IntToStr(i+1);
            Res := UpdateSurfaceElement(TableScene1.text, StringObjects[i].text, 1, 1, nil, PWideChar(TextIter), nil,
                                    INT_UNDEF, INT_UNDEF, INT_UNDEF, INT_UNDEF, FLT_UNDEF, 0, @ErrData);
            if Res <> JTP_OK then
              begin
                ErrStr := TJTPUtils.GetErrorString(Res, ErrData.pErrorsStr);
                Result := TJsonUtils.StringToResult(Res, ErrStr, 'UpdateSurfaceElement');
                Exit;
              end;

          end;

        Res := JTP_OK;
        ErrStr := '';
      end
                                     else
      begin
        Res := JTP_INTERNAL_ERROR;
        ErrStr := 'Internal error';
      end;

    Result := TJsonUtils.StringToResult(Res, ErrStr, 'Play');
  finally
    CloseRecords(0, nil);
  end;
end;

end.


unit uJSONUtils;
{$i ..\JTPStudio\JTPProjects.inc}

interface

uses
  System.SysUtils, System.Classes,
  System.JSON.Types,
  System.JSON.Writers, System.JSON.Readers, System.JSON.Builders,
  TypesJTP, JTPStudio, Decklink;

resourcestring
  rsRes = 'res';
  rsErrorStr = 'errorStr';
  rsResObj = 'resObj';
  rsList = 'list';
  rsData = 'data';
  rsListSeparator = '---';

resourcestring
  rsErr_EnumerateVideoModes = 'EnumerateVideoModes: ���������� ������ �������.';

type
  TJTPJsonResult = string;
  TStrArr = array[1..MAX_VIDEO_DEVICES+1] of Str256;

  TJsonUtils = class
  public
    class function GetErrorString(const Res: UInt64; PErrStr: PAnsiChar): string;
    class function StringArrToResult(const Res: UInt64;
        const ErrStr: string; const ResList: TStrArr): TJTPJsonResult;
    class function StringListToResult(const Res: UInt64;
        const ErrStr: string; const ResList: TStrings): TJTPJsonResult;
    class function StringToResult(const Res: UInt64;
        const ErrStr: string; const ResString: string): TJTPJsonResult;
  end;

  TJsonStringWriter = class(TJsonTextWriter)
  private
    FStringWriter: TStringWriter;
  public
    constructor Create;
    destructor Destroy; override;
    function ToString: string; override;
  end;

  TJsonStringBuilder = class(TJsonObjectBuilder)
  private
    FStringWriter: TStringWriter;
    FJsonWriter: TJsonTextWriter;
  public
    constructor Create;
    destructor Destroy; override;
    function ToString: string; override;
    property JsonTextWriter: TJsonTextWriter read FJsonWriter;
  end;

implementation

uses
  System.AnsiStrings;

{ TJsonUtils }

class function TJsonUtils.GetErrorString(const Res: UInt64; PErrStr: PAnsiChar): string;
var
  ln: Cardinal;
begin
  if Res = JTP_OK then
    begin
      Result := '';
      Exit;
    end;
  ln := System.AnsiStrings.StrLen(PErrStr);
  if ln > 0
    then SetString(Result, PErrStr, ln)
    else Result := '';
end;

class function TJsonUtils.StringArrToResult(const Res: UInt64;
  const ErrStr: string; const ResList: TStrArr): TJTPJsonResult;
var
  SL: TStringList;
  i: integer;
  ln: Cardinal;
  str: string;
begin
  Result := '';
  SL := TStringList.Create;
  try
    for i:=1 to MAX_VIDEO_DEVICES do
      begin
        ln := System.AnsiStrings.StrLen(@ResList[i].text[0]);
        if ln = 0 then break;
        SetString(str, PAnsiChar(@ResList[i].text[0]), ln);
        SL.Add(str);
      end;

    Result := TJsonUtils.StringListToResult(Res, ErrStr, SL);
  finally
    SL.Free;
  end;
end;

class function TJsonUtils.StringListToResult(const Res: UInt64;
  const ErrStr: string; const ResList: TStrings): TJTPJsonResult;
var
  Builder: TJsonStringBuilder;
  i: integer;
  arr: TJSONCollectionBuilder.TElements;
begin
  Result := '';
  Builder := TJsonStringBuilder.Create;
  try
    arr := Builder
      .BeginObject
        .Add(rsRes, Res)
        .Add(rsErrorStr, ErrStr)
        .BeginObject(rsResObj)
          .BeginArray(rsList);

    for i:=0 to ResList.Count-1 do
      arr.Add(ResList[i]);

    arr
          .EndArray
        .EndObject
      .EndObject;

    Result := Builder.ToString;
  finally
    Builder.Free;
  end;
end;

class function TJsonUtils.StringToResult(const Res: UInt64;
  const ErrStr: string; const ResString: string): TJTPJsonResult;
var
  Builder: TJsonStringBuilder;
begin
  Result := '';
  Builder := TJsonStringBuilder.Create;
  try
    Builder
      .BeginObject
        .Add(rsRes, Res)
        .Add(rsErrorStr, ErrStr)
        .BeginObject(rsResObj)
          .Add(rsData, ResString)
        .EndObject
      .EndObject;

    Result := Builder.ToString;
  finally
    Builder.Free;
  end;
end;

{ TJsonStringWriter }

constructor TJsonStringWriter.Create;
begin
  FStringWriter := TStringWriter.Create;
  inherited Create(FStringWriter)
end;

destructor TJsonStringWriter.Destroy;
begin
  FStringWriter.Free;
  inherited Destroy;
end;

function TJsonStringWriter.ToString: string;
begin
  Result := FStringWriter.ToString;
end;

{ TJsonStringBuilder }

constructor TJsonStringBuilder.Create;
begin
  FStringWriter := TStringWriter.Create;
  FJsonWriter := TJsonTextWriter.Create(FStringWriter);
  inherited Create(FJsonWriter);
end;

destructor TJsonStringBuilder.Destroy;
begin
  FJsonWriter.Free;
  FStringWriter.Free;
  inherited Destroy;
end;

function TJsonStringBuilder.ToString: string;
begin
  Result := FStringWriter.ToString;
end;

end.

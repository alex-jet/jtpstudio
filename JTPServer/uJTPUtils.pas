unit uJTPUtils;
{$i ..\JTPStudio\JTPProjects.inc}

interface

uses
  System.SysUtils, System.Classes,
  System.JSON.Types,
  System.JSON.Writers, System.JSON.Readers, System.JSON.Builders,
  TypesJTP;

type
  TJTPUtils = class
  public
    class function GetErrorString(const Res: UInt64; pErrStrs: PAnsiChar): string;
    class procedure GetErrorStrings(const Res: UInt64; pErrStrs: PAnsiChar;
                                    const AList: TStrings);
    class function StringFromPStrs(pStrs: PAnsiChar): string;
    class procedure StringsFromPStrs(pStrs: PAnsiChar; const AList: TStrings);
    class procedure StringToAnsiArray(const Source: UnicodeString;
            const Arr: PAnsiChar; const BuffSize: Cardinal);
    class function StringToAnsiString(const Source: UnicodeString): AnsiString;
  end;

  EJTPException = class(Exception)
  private
    FRes: UInt64;
  public
    constructor Create(const ARes: UInt64; pErrStrs: PAnsiChar);
    property Res: UInt64 read FRes;
  end;

implementation

uses
  System.AnsiStrings;

{ TJTPUtils }

class function TJTPUtils.GetErrorString(const Res: UInt64;
  pErrStrs: PAnsiChar): string;
begin
  if Res = JTP_OK then
    begin
      Result := '';
      Exit;
    end;
  Result := TJTPUtils.StringFromPStrs(pErrStrs);
end;

class procedure TJTPUtils.GetErrorStrings(const Res: UInt64;
  pErrStrs: PAnsiChar; const AList: TStrings);
begin
  if Res = JTP_OK then
    begin
      AList.Clear;
      Exit;
    end;
  TJTPUtils.StringsFromPStrs(pErrStrs, AList);
end;

class function TJTPUtils.StringFromPStrs(pStrs: PAnsiChar): string;
var
  SL: TStringList;
begin
  Result := '';
  SL := TStringList.Create;
  try
    TJTPUtils.StringsFromPStrs(pStrs, SL);
    if SL.Count > 0
      then Result := SL.Text;
  finally
    SL.Free;
  end;
end;

class procedure TJTPUtils.StringsFromPStrs(pStrs: PAnsiChar;
  const AList: TStrings);
var
  ln: Cardinal;
  s: string;
begin
  AList.Clear;
  ln := System.AnsiStrings.StrLen(pStrs);
  while ln > 0 do
    begin
      SetString(s, pStrs, ln);
      AList.Add(s);
      pStrs := pStrs + ln*SizeOf(AnsiChar) + 1;
      ln := System.AnsiStrings.StrLen(pStrs);
    end;
end;

class procedure TJTPUtils.StringToAnsiArray(const Source: UnicodeString;
  const Arr: PAnsiChar; const BuffSize: Cardinal);
var
  strLen: Cardinal;
  CP: Cardinal;
begin
  CP := 65001;
  strLen := LocaleCharsFromUnicode(CP, 0, PWideChar(Source), Length(Source), nil, 0, nil, nil);
  if strLen > 0 then
  begin
    if strLen > BuffSize then strLen := BuffSize;
    LocaleCharsFromUnicode(CP, 0, PWideChar(Source), Length(Source), Arr, strLen, nil, nil);
  end;
end;

class function TJTPUtils.StringToAnsiString(const Source: UnicodeString): AnsiString;
var
  strLen: Cardinal;
  CP: Cardinal;
begin
  CP := 65001;
  strLen := LocaleCharsFromUnicode(CP, 0, PWideChar(Source), Length(Source), nil, 0, nil, nil);
  if strLen > 0 then
  begin
    SetLength(Result, strLen);
    LocaleCharsFromUnicode(CP, 0, PWideChar(Source), Length(Source), PAnsiChar(Result), strLen, nil, nil);
    CP := 1251;
    SetCodePage(PRawByteString(@Result)^, CP, False);
  end;
end;

{ EJTPException }

constructor EJTPException.Create(const ARes: UInt64; pErrStrs: PAnsiChar);
var
  ErrS: string;
begin
  ErrS := TJTPUtils.GetErrorString(ARes, pErrStrs);
  inherited Create(ErrS);
  FRes := ARes;
end;

end.

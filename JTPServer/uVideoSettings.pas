unit uVideoSettings;
{$i ..\JTPStudio\JTPProjects.inc}

interface

uses
  WinAPI.Windows,
  System.SysUtils, System.Variants, System.Classes, System.Json,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.ComCtrls,
  TypesJTP, JTPStudio, Decklink,
  fVideoOutForm;

type
  TStrArr = array[1..MAX_VIDEO_DEVICES+1] of Str256;

  TTrunkSettingsRec = record
    Active: boolean;
    DeviceIdx: integer;
    ModeIdx: integer;
    KeyPass: boolean;
  end;

  TTrunkSettingsArr = array[1..MAX_VIDEO_TRUNKS] of TTrunkSettingsRec;

  TVideoSettingsRec = record
    OutSim: boolean;
    TrunkSets: TTrunkSettingsArr;
  end;

  TVideoSettingsObj = class(TObject)
  private
    FVersion: integer;
    FFlags: integer;
    FStarted: boolean;
    FVideoTrunks: array[1..MAX_VIDEO_TRUNKS] of TVideoTrunk;
    FTrunks: array[1..MAX_VIDEO_TRUNKS] of integer;
    FOutForms: array[1..MAX_VIDEO_TRUNKS] of TVideoOutForm1;
    procedure StringArrToList(const Arr: TStrArr; const AList: TStrings);
    function VideoModeFromIndex(Indx: integer): integer;
  public
    constructor Create;
    destructor Destroy; override;
    procedure GetVideoDevicesList(const DeviceType: dword; const AList: TStrings);
    procedure GetVideoModesList(const AList: TStrings);
    procedure InitVideo(const VideoSets: TVideoSettingsRec);
    procedure StopEngine;
    property Version: integer read FVersion;
    property Flags: integer read FFlags write FFlags;
  end;

implementation

uses
  System.AnsiStrings,
  uJTPUtils;

{ TVideoSettingsObj }

constructor TVideoSettingsObj.Create;
var
  i: byte;
begin
  inherited Create;

  SetMXCSR($1F80);

  FVersion := 1;
  FFlags := 0;
  for i:=1 to MAX_VIDEO_TRUNKS do
    begin
      FTrunks[i] := -i;
      FOutForms[i] := TVideoOutForm1.Create(nil);
      FOutForms[i].Caption := '����� '+IntToStr(i);
    end;
end;

destructor TVideoSettingsObj.Destroy;
var
  i: byte;
begin
  StopEngine;

  for i:=1 to MAX_VIDEO_TRUNKS do
    begin
      FOutForms[i].Free;
    end;

  inherited Destroy;
end;

procedure TVideoSettingsObj.GetVideoDevicesList(const DeviceType: dword;
  const AList: TStrings);
var
  Res: UInt64;
  EnumDeviceErr: TJtpFuncData;
  EnumParams: dword;
  DeviceNames: TStrArr;
begin
  if not Assigned(AList) then Exit;
  EnumParams := 0;
  if DeviceType = SIMULATOR_VIDEO_DEVICE
    then EnumParams := EnumParams or EnumVidDevsNoWarn;
  Res := EnumerateVideoDevices(DeviceType, @DeviceNames, EnumParams, @EnumDeviceErr);
  if Res <> JTP_OK then
    raise EJTPException.Create(Res, EnumDeviceErr.pErrorsStr);
  StringArrToList(DeviceNames, AList);
end;

procedure TVideoSettingsObj.GetVideoModesList(const AList: TStrings);
begin
  AList.Add('HD1080i50');   // Indx = 0
  AList.Add('PAL');         // Indx = 1
  AList.Add('4K2160p25');   // Indx = 2
  AList.Add('4K2160p50');   // Indx = 3
  AList.Add(''); // Indx = 4
  AList.Add('HD1080p24');   // Indx = 5
  AList.Add('HD1080p25');   // Indx = 6
  AList.Add('HD1080p30');   // Indx = 7
  AList.Add('HD1080p50');   // Indx = 8
  AList.Add('HD1080p6000'); // Indx = 9
  AList.Add(''); // Indx = 10
  AList.Add('HD1080i6000'); // Indx = 11
  AList.Add(''); // Indx = 12
  AList.Add('4K2160p24');   // Indx = 13
  AList.Add('4K2160p30');   // Indx = 14
  AList.Add('4K2160p60');   // Indx = 15
  AList.Add(''); // Indx = 16
  AList.Add('HD720p50');    // Indx = 17
  AList.Add('HD720p60');    // Indx = 18
  AList.Add(''); // Indx = 19
  AList.Add('NTSC');        // Indx = 20
end;

procedure TVideoSettingsObj.InitVideo(const VideoSets: TVideoSettingsRec);
var
  Res: UInt64;
  iTrunk: byte;
  pTrunk: pVideoTrunk;
  InitVideoDesc: TInitVideoDesc;
begin
  StopEngine;

  for iTrunk:=1 to MAX_VIDEO_TRUNKS do
    begin
      pTrunk := @FVideoTrunks[iTrunk];

      pTrunk.ClearForDecklink();
      if not VideoSets.TrunkSets[iTrunk].Active then
        begin
          FTrunks[iTrunk] := -iTrunk;
          Continue;
        end;
      FTrunks[iTrunk] := iTrunk;

      pTrunk.ScreensHorizontal := 1;
      pTrunk.ScreensVertical := 1;
      pTrunk.hOutWindow := FOutForms[iTrunk].VideoOutHandle;
      pTrunk.OutputDisplayMode := VideoModeFromIndex(VideoSets.TrunkSets[iTrunk].ModeIdx);
      pTrunk.OutDevices[0] := VideoSets.TrunkSets[iTrunk].DeviceIdx + 1;
      if VideoSets.TrunkSets[iTrunk].KeyPass then
        pTrunk.Flags := pTrunk.Flags or TrunkVideo_KeyPass;
    end;

  with InitVideoDesc do begin
    Version := 1;
    VerticalBlur := 0.15;
 	  ScreenAlpha := 1.0;
    ClearColor := $00999999;
  end;

  Res := JTPStudio.InitVideo(@FVideoTrunks, 0, @InitVideoDesc);
  if Res <> JTP_OK then
    raise EJTPException.Create(Res, InitVideoDesc.pErrorsStr);

  for iTrunk:=1 to MAX_VIDEO_TRUNKS do
    if FTrunks[iTrunk] > 0
      then FOutForms[iTrunk].Show;

  FStarted := true;
end;

procedure TVideoSettingsObj.StopEngine;
var
  iTrunk: byte;
begin
  if FStarted then
    begin
      CloseEngine(0);
      for iTrunk:=1 to MAX_VIDEO_TRUNKS do
        FOutForms[iTrunk].Close;
      FStarted := false;
    end;
end;

procedure TVideoSettingsObj.StringArrToList(const Arr: TStrArr; const AList: TStrings);
var
  i: integer;
  ln: Cardinal;
  s: string;
begin
  if not Assigned(AList) then Exit;
  for i:=1 to MAX_VIDEO_DEVICES do
    begin
      ln := System.AnsiStrings.StrLen(@Arr[i].text[0]);
      if ln = 0 then break;
      SetString(s, PAnsiChar(@Arr[i].text[0]), ln);
      AList.Add(s);
    end;
end;

function TVideoSettingsObj.VideoModeFromIndex(Indx: integer): integer;
begin
  Result := -1;
  case Indx of
     0: Result := bmdModeHD1080i50;
     1: Result := bmdModePAL;
     2: Result := bmdMode4K2160p25;
     3: Result := bmdMode4K2160p50;

     5: Result := bmdModeHD1080p24;
     6: Result := bmdModeHD1080p25;
     7: Result := bmdModeHD1080p30;
     8: Result := bmdModeHD1080p50;
     9: Result := bmdModeHD1080p6000;

    11: Result := bmdModeHD1080i6000;

    13: Result := bmdMode4K2160p24;
    14: Result := bmdMode4K2160p30;
    15: Result := bmdMode4K2160p60;

    17: Result := bmdModeHD720p50;
    18: Result := bmdModeHD720p60;

    20: Result := bmdModeNTSC;
  end;
end;

end.

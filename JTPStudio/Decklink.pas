unit Decklink;

interface

//  ����� ������ Decklink
const
  bmdModeNTSC	         : integer = $6e747363;
  bmdModeNTSC2398	     : integer = $6e743233;
  bmdModePAL	         : integer = $70616c20;
  bmdModeNTSCp	       : integer = $6e747370;
  bmdModePALp	         : integer = $70616c70;
  bmdModeHD1080p2398	 : integer = $32337073;
  bmdModeHD1080p24	   : integer = $32347073;
  bmdModeHD1080p25	   : integer = $48703235;
  bmdModeHD1080p2997	 : integer = $48703239;
  bmdModeHD1080p30	   : integer = $48703330;
  bmdModeHD1080i50	   : integer = $48693530;
  bmdModeHD1080i5994	 : integer = $48693539;
  bmdModeHD1080i6000	 : integer = $48693630;
  bmdModeHD1080p50	   : integer = $48703530;
  bmdModeHD1080p5994	 : integer = $48703539;
  bmdModeHD1080p6000	 : integer = $48703630;
  bmdModeHD720p50	     : integer = $68703530;
  bmdModeHD720p5994	   : integer = $68703539;
  bmdModeHD720p60	     : integer = $68703630;
  bmdMode2k2398	       : integer = $326b3233;
  bmdMode2k24	         : integer = $326b3234;
  bmdMode2k25	         : integer = $326b3235;
  bmdMode2kDCI2398	   : integer = $32643233;
  bmdMode2kDCI24	     : integer = $32643234;
  bmdMode2kDCI25	     : integer = $32643235;
  bmdMode4K2160p2398   : integer = $346b3233;
  bmdMode4K2160p24	   : integer = $346b3234;
  bmdMode4K2160p25	   : integer = $346b3235;
  bmdMode4K2160p2997   : integer = $346b3239;
  bmdMode4K2160p30	   : integer = $346b3330;
  bmdMode4K2160p50	   : integer = $346b3530;
  bmdMode4K2160p5994   : integer = $346b3539;
  bmdMode4K2160p60	   : integer = $346b3630;
  bmdMode4kDCI2398	   : integer = $34643233;
  bmdMode4kDCI24	     : integer = $34643234;
  bmdMode4kDCI25	     : integer = $34643235;
  bmdModeUnknown	     : integer = $69756e6b;

//  ������� ������� Decklink
const
  bmdFormat8BitYUV	   : integer = $32767579;
  bmdFormat10BitYUV    : integer = $76323130;
  bmdFormat8BitARGB    : integer = 32;
  bmdFormat8BitBGRA    : integer = $42475241;
  bmdFormat10BitRGB    : integer = $72323130;
  bmdFormat12BitRGB	   : integer = $52313242;
  bmdFormat12BitRGBLE	 : integer = $5231324c;
  bmdFormat10BitRGBXLE : integer = $5231306c;
  bmdFormat10BitRGBX	 : integer = $52313062;
  bmdFormatH265	       : integer = $68657631;
  bmdFormatDNxHR	     : integer = $41566468;

implementation

end.

{$define devM}
{$ifdef devM}
  {$undef devM}
  {$ifndef jtp_dev_mode}
    {$define jtp_dev_mode}
  {$endif}
  {$ifdef jtp_release_mode}
    {$undef jtp_release_mode}
  {$endif}
{$else}
  {$ifdef jtp_dev_mode}
    {$undef jtp_dev_mode}
  {$endif}
  {$ifndef jtp_release_mode}
    {$define jtp_release_mode}
  {$endif}
{$endif}
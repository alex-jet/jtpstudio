unit JTPStudio;

interface

uses
  Windows,
  TypesJTP, Decklink;


const  JTPStudioPath = 'C:\Program Files\JTPStudio\';
const  JTPStudioDLL = JTPStudioPath + 'JTPStudioNew.dll';


const  MAX_VIDEO_DEVICES = 16;    //  ������������ ���-�� ����� ���������.
const  LAST_VIDEO_DEVICE = MAX_VIDEO_DEVICES - 1;

const  MAX_VIDEO_TRUNKS = 4;    //  ������������ ���-�� �������.
const  LAST_VIDEO_TRUNK = MAX_VIDEO_TRUNKS - 1;

const  MAX_PREVIEW_WINDOWS = 4;


//     Video device types.
const  DECKLINK_VIDEO_DEVICE      : DWORD = $00000001;
const  BLUEFISH_VIDEO_DEVICE      : DWORD = $00000002;
const  MATROX_VIDEO_DEVICE        : DWORD = $00000004;
const  DIRECTSHOW_VIDEO_DEVICE    : DWORD = $00000008;
const  SIMULATOR_VIDEO_DEVICE     : DWORD = $00000010;



//   VideoTrunk. ����� ����� ������.
Type TVideoTrunk = record

  Size               : integer;    //  ������ ���-�� TVideoTrunk.

//  ScreensLayout      : integer;    //  ������������ ������� � ������: 1� ������ (TrunkScreens_1x1), 2�1 ������� (TrunkScreens_2x1) � �.�.
  ScreensHorizontal  : integer;    //  ���-�� ������� �� �����������.
  ScreensVertical    : integer;    //  ���-�� ������� �� ���������.

  ScreensRotate      : integer;    //  �������� ������. ��������, TrunkVideo_90R � �.�.

  Flags              : integer;    //  ��������� �����. ��������, TrunkVideo_SD_16_9 � �.�.

  OutputDisplayMode  : integer;    //  ����� ����� ������. ��������, bmdModeHD1080i50, bmdModePAL � �.�.
  OutputPixelFormat  : integer;    //  ������ ������� �� �����. ��������, bmdFormat8BitBGRA  � �.�.
  hOutWindow         : HWND;       //  ���� ������ ����� ��������.

  InputDisplayMode   : integer;    //  ����� ������� �����. ��������, bmdModeHD1080i50, bmdModePAL � �.�.
  InputPixelFormat   : integer;    //  ������ ������� �� ����. ��������, bmdFormat8BitBGRA  � �.�.

  AlphaChannelTrunk  : integer;    //  ������ ������, ������������� ��� �����-�����. -1 - �� ������������.

  GpuIndex           : integer;    //  ����� GPU � ������� ������������ ��� ������� ������.

  pExtensions        : pointer;    //  ���������� ��� ������������� ���������. ��������, ��������� VideoCapture (������ �� ����� �����)

  OutDevices   : array [0..LAST_VIDEO_DEVICE] of integer;
  InputDevices : array [0..LAST_VIDEO_DEVICE] of integer;

  Reserves     : array [0..127] of integer;   //  ������.

  FrameWidthFactor   : integer;    //  �� ������������.
  Rect               : TRect;      //  �� ������������.
  pWindowRender      : pointer;    //  �� ������������.

  procedure ClearForDecklink();
end;

PVideoTrunk = ^TVideoTrunk;



//    ������������ ������� � ������ (��� TVideoTrunk.ScreensLayout).
{const TrunkScreens_1x1    : integer = $00000001;  //  ���� ������� � ������.
const TrunkScreens_2x1    : integer = $00000002;  //  ��� �������� � ������ �������������.
const TrunkScreens_3x1    : integer = $00000004;  //  ...
const TrunkScreens_4x1    : integer = $00000008;
const TrunkScreens_1x2    : integer = $00000010;
const TrunkScreens_1x3    : integer = $00000020;
const TrunkScreens_1x4    : integer = $00000040;
const TrunkScreens_2x2    : integer = $00000080;}

//    �������� ������� � ������ (��� TVideoTrunk.ScreensRotate).
const TrunkScreens_90R    : integer = $00000001;   //  ����������� ������.
const TrunkScreens_90L    : integer = $00000002;   //  ����������� �����.
const TrunkScreens_180    : integer = $00000004;   //  ����������� �� 180 ��������.

//    ����� ��� TVideoTrunk.Flags
const TrunkVideo_SD_16_9  : integer = $00000001;   //  ����� SD (PAL 4/3) ������������� �� 16/9.
const TrunkVideo_KeyPass  : integer = $00000002;   //  ����� �� ������.
const TrunkVideo_VertBlur : integer = $00000004;   //  ������ ������������ ���� (��� ����� ��� �����).

//    ����� GPU �� ��������� ��� �������.
const TrunkVideo_GpuDefault : integer = -1;


Type PHWnd = ^HWND;


const MaxErrorString = 4096;

Type TJtpFuncData = object
  pErrorsStr : PAnsiChar;
end;
PJtpFuncData = ^TJtpFuncData;



//////////////   P R O X Y    E N G I N E    F U N C T I O N S     ///////////////

function Get3DEngineVersion() : UInt64;  stdcall;  external JTPStudioDLL;
//  �������� ������ ������.



function EnumerateVideoDevices( DeviceType: DWORD;  DevicesNames: PStr256;  Params: DWORD;  pFuncData: pointer ) : UInt64;  stdcall;  external JTPStudioDLL;
//  ���������� ����� ����� ����. � ������ ������ ���������� OK.
//  � ��������� DevicesNames ���������� 0.
//  Params:
          const EnumVidDevsNoWarn : DWORD = 1;    //  �� ���������� ��������������, ���� �� ������� ����� ����.



function InitVideo( VideoTrunks: PVideoTrunk;  Params: DWORD;  pInitVideoDesc: pointer) : UInt64;  stdcall;  external JTPStudioDLL;
//  ������������� ������������ ����� ������� � �������������� ���������� � ���.
		//  VideoTrunks - ������ ����� ������/�����.
		//  Params - �� ������������
		//  pInitVideoDesc - ��������� ��
  		  type TInitVideoDesc = record
	    		Version : integer;          //  ������.
	    		VerticalBlur : single;      //  ���� �������� ��� interlaced-����������.
		    	ScreenAlpha : single;       //  ������������ ���� ��������.
	  	    ClearColor : dword;
          pErrorsStr : PAnsiChar;     //  ����� ������, ���� ���������.
		    end;



function LoadProject( ProjectPath: PPath;  TexturePathes: PPath;  Params: DWORD;  pLoadProjectData: pointer ) : UInt64;  stdcall;  external JTPStudioDLL;
//  ��������� ������ ����������.
		//  ProjectPath - ���� �������, ��� ��������� ���� � ������ � ���������� �������.
		//  TexturePathes - ���� � ��������� (����, �������� � �.�.). � ��������� TexturePathes ���������� 0.



function StartEngine( Version: integer;  Params: DWORD;  pStartEngineData: pointer ) : UInt64;  stdcall;  external JTPStudioDLL;
//  �������� ������.
		//  Version = 1.


function CloseEngine( Params: DWORD ) : UInt64;  stdcall;  external JTPStudioDLL;
//  ��������� ������.
    //  Params - �� ������������.



function OpenRecords( Params: DWORD;  pReserve: pointer ) : UInt64;  stdcall;  external JTPStudioDLL;
//  ��������� ������ �������� ��� ������.

function CloseRecords( Params: DWORD;  pReserve: pointer ) : UInt64;  stdcall;  external JTPStudioDLL;
//  ��������� ������ �������� ��� ������.



function PlayScene( SceneName: PAnsiChar;  Delay: single;  Mode: dword;  TrunkIndex: integer;  pPlaySceneData: pointer ) : UInt64;  stdcall;  external JTPStudioDLL;
//  ��������� �����.
		//  SceneName  - ��� �����.
		//  Delay      - ��������, ����� ����� �������� �� ������.
    //  Mode       - ����� (�� ������������).
    //  TrunkIndex - ������ ������ (�� 1 �� MAX_VIDEO_TRUNKS).
    //  pPlaySceneData - ��������� ��

		Type TPlaySceneData = object(TJtpFuncData)
		  SceneName: TName;   //  ��� ��������� �����. �������� ����� ���������� 'MyScene_Trunk1'.
		  hPreviewWnds : array[0..MAX_PREVIEW_WINDOWS-1] of HWND;  //  ���� ��� ������.
      Constructor Create;
		end;
    PPlaySceneData = ^TPlaySceneData;



function CloseScene( SceneName: PAnsiChar;  Delay: single;  pPlaySceneData : PJtpFuncData ) : dword;   stdcall; external JTPStudioDLL;
//  ��������� �����.
		//  Delay - �������� �� �������� �����. ���� FLT_UNDEF - �������� ������ �� ������ �����.



function CloneObject( SceneName, ObjectName: PAnsiChar;  pPos, pOrient, pSize, pColor : PVector;  Delay : single;  Flags: dword;  pCloneObjectData: pointer ) : UInt64;   stdcall; external JTPStudioDLL;
//  ��������� ������.  ����� �������� ������������ ������������, ���� nil, �� �� ���������.
		//  Delay - �������� ������������.
    //  pPos, pOrient, pSize, pColor - ��������� �� �������. ��������, ������� ������������ ��������� �������.
    //                               - pSize, pColor : ������ � ���� ������ �������.
    //  ���� �����-���� ��������� nil, �� �� �����������. ���� �����-���� ��������� ������� ����� FLT_UNDEF, �� ��������� �� �����������.

    Type TCloneObjectData = object(TJtpFuncData)
		  ObjectName: TName;   //  ��� ����������� �������.
      Constructor Create;
		end;
    PCloneObjectData = ^TCloneObjectData;



function UpdateSurfaceElement( SceneName, ObjectName: PAnsiChar;  Index, NumTex: integer;  PicturePath : PAnsiChar;  Text: PWideChar;  pColor : PColor;
                               X,Y, W,H: integer;  Delay: single;  Flags: dword;   pUpdateSurfElemData : PJtpFuncData ) : UInt64;   stdcall; external JTPStudioDLL;
//  �������� ������� �����������. ��� ����� ���� ����� ��� ������.
    //  SceneName, ObjectName - ����� ����� � �������.
    //  Index - ����� ���������, NumTex - ����� ��������. ������� �� 1-��.
		//  ���� PicturePath <> nil, ����������� ��������.  ���� Text <> nil, ����������� �����.
		//  ���� pColor = nil, �� ���� �� �����������. ���� ��������� ����� ����� FLT_UNDEF, �� �� �����������.
    //  ���� X,Y,W,H �� ����� INT_UNDEF - �������������� �����������.
		//  Delay = FLT_UNDEF - �������� ������ �� ������ �����.
    //  Flags = 0.



//////////////////////////////////////////////////////////////////////////////////



implementation


Constructor TPlaySceneData.Create;
var i : integer;
begin
	SceneName.text[0] := #0;
  for i := 0 to (MAX_PREVIEW_WINDOWS-1) do
    hPreviewWnds[i] := 0;
end;


Constructor TCloneObjectData.Create;
begin
	ObjectName.text[0] := #0;
end;



procedure TVideoTrunk.ClearForDecklink();
var device : integer;
    i : integer;
begin
  Size := sizeof( TVideoTrunk );

//  ScreensLayout := 0;
  ScreensHorizontal := 0;
  ScreensVertical := 0;
  ScreensRotate := 0;
  Flags := 0;

  OutputDisplayMode := 0; //bmdModeUnknown;
  OutputPixelFormat := bmdFormat8BitBGRA;
  hOutWindow := 0;

  InputDisplayMode := 0;  //bmdModeUnknown;
  InputPixelFormat := bmdFormat8BitBGRA;

  AlphaChannelTrunk := -1;

  GpuIndex := TrunkVideo_GpuDefault;

  pExtensions := nil;

  FrameWidthFactor := 0;

  with Rect do begin
    x1 := 0;  x2 := 0;  y1 := 0;  y2 := 0;
  end;

  for device := 0 to LAST_VIDEO_DEVICE do begin
    OutDevices[device] := -1;
    InputDevices[device] := -1;
  end;

  for i := 0 to 127 do
    Reserves[i] := 0;
end;

end.
